<?php
/**
 * Header Tabs
 * Magic Words
 *
 * @license		GNU General Public License v2.0 or later
 * @package		HeaderTabs
 * @link		https://www.mediawiki.org/wiki/Extension:Header_Tabs
 *
**/

$magicWords = [];

/** English (English) */
$magicWords['en'] = [
	'switchtablink' => [0, 'switchtablink'],
];
