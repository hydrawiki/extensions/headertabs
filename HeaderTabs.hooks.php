<?php
/**
 * Header Tabs
 * Hooks
 *
 * @license		GNU General Public License v2.0 or later
 * @package		HeaderTabs
 * @link		https://www.mediawiki.org/wiki/Extension:Header_Tabs
 *
**/

class HeaderTabsHooks {
	/**
	 * Add in the <headertabs/> tag and {{#switchtablink:}} parser function.
	 *
	 * @see https://www.mediawiki.org/wiki/Manual:Hooks/ParserFirstCallInit
	 * @param Parser $parser
	 * @return true
	 */
	public static function onParserFirstCallInit(Parser &$parser) {
		$parser->setHook('headertabs', ['HeaderTabs', 'tag']);
		$parser->setFunctionHook('switchtablink', ['HeaderTabs', 'renderSwitchTabLink']);
		$parser->mShowToc = false;

		return true;
	}

	/**
	 * Do very minor modifcation to source. Wrapping "tabs" in a
	 * div for easier processing in javascript, and addMapping
	 * Section tags around each "tab section".
	 */
	public static function onParserAfterTidy( &$parser, &$text ) {
		return HeaderTabs::modifyOutput( $parser, $text );
	}

	public static function onResourceLoaderGetConfigVars( &$vars ) {
		global $wgHeaderTabsUseHistory,
			   $wgHeaderTabsEditTabLink,
			   $wgHeaderTabsStyle,
			   $wgHeaderTabsRenderSingleTab,
			   $wgHeaderTabsDisableDefaultToc,
			   $wgHeaderTabsGenerateTabTocs;

		$vars['wgHeaderTabsUseHistory'] = $wgHeaderTabsUseHistory;
		$vars['wgHeaderTabsEditTabLink'] = $wgHeaderTabsEditTabLink;
		$vars['wgHeaderTabsStyle'] = $wgHeaderTabsStyle;
		$vars['wgHeaderTabsRenderSingleTab'] = $wgHeaderTabsRenderSingleTab;
		$vars['wgHeaderTabsDisableDefaultToc'] = $wgHeaderTabsDisableDefaultToc;
		$vars['wgHeaderTabsGenerateTabTocs'] = $wgHeaderTabsGenerateTabTocs;

		return true;
	}

	/**
	 * @param $out OutputPage
	 * @return bool
	 */
	public static function onBeforePageDisplay( &$out ) {
		global $wgScriptPath, $wgHeaderTabsStyle;
		$out->addModules( 'ext.headertabs.css' );
		$out->addModules( 'ext.headertabs.js' );
		return true;
	}

}
