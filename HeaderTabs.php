<?php
/**
 * Header Tabs
 * Turn table of contents into tabbed contents using standard headers.
 *
 * @license		GNU General Public License v2.0 or later
 * @package		HeaderTabs
 * @link		https://www.mediawiki.org/wiki/Extension:Header_Tabs
 *
**/

if (function_exists('wfLoadExtension')) {
	wfLoadExtension('HeaderTabs');
	// Keep i18n globals so mergeMessageFileList.php doesn't break
	$wgMessagesDirs['HeaderTabs'] = __DIR__ . '/i18n';
	wfWarn(
		'Deprecated PHP entry point used for HeaderTabs extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
 } else {
	die('This version of the HeaderTabs extension requires MediaWiki 1.25+');
}
