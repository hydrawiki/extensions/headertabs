/**
 *  Header Tabs Self Executing Function.
 */

$(function(){
	///////////////////////
	// Setup and Parsing //
	///////////////////////

	// Setup container references / crate containers
	var $cont = $("#headerTabsContainer"); // Parse This
	var $ht = $('<div/>',{
		id: 'headerTabs',
		class: mw.config.get('wgHeaderTabsStyle')
	}); // Into This
	var $titles = $('<ul/>');
	var $toc = $('#toc');

	// Edit link storage.
	var editLinkStore = {};

	var $sections = $cont.children('section');

	if ($sections.length < 1 || ($sections.length == 1 && mw.config.get('wgHeaderTabsRenderSingleTab') == false)) {
		// bail out
		return;
	}

	// Loop through each section tag and parse it.
	$sections.each(function(i){
		// Get Title for Tab
		var title = $(this).find("span.mw-headline").html();
		// Get the ID, used to make hashes
		var id = $(this).find("span.mw-headline").attr('id');
		// Some strange issue causes a section to be itenditifed needing to be tabbed,
		// but isn't actually the case? Let's just verify we found an id to reference
		// for our hash. If not, dont attempt tab creation.
		if (id) {
			// Store the edit links for use later
			editLinkStore['#'+id] = $('<div>').append($(this).find(".mw-editsection").first().clone()).html();
			// Builds "tabs" list items
			$titles.append('<li><a href="#tabs-'+i+'">'+title+'</a></li>')
			// Pull sections (non header) content and append it to the header tabs div
			var $sectionContent = $(this);
			$sectionContent.children('h1').remove();
			var content = $sectionContent.html();
			// Generate TOC in Tab?
			if(mw.config.get('wgHeaderTabsGenerateTabTocs')) {
				var embedToc = $toc.find('a[href="#'+id+'"]').siblings('ul').html();
				if (embedToc && embedToc.length) {
					embedToc = '<div id="toc_'+id+'" class="toc">'
							 + '<div id="toctitle_'+id+'"><h2>Contents</h2></div>'
							 + '<ul>'
							 + embedToc
							 + '</ul>'
							 + '</div>';
					content = embedToc + content;
				}
			}
			$ht.append('<div id="tabs-'+i+'" data-hash="#'+id+'">'+content+'</div>');
		}
	});

	// Prepend titles / tabs unordered list element to headertabs div.
	$ht.prepend($titles);

	// create a edit section container for the tabs, and add it to headertabs.
	var $htedit = $('<span/>',{
		class: "ht-editsection",
		id: "edittab"
	});
	$ht.prepend($htedit);

	// Place headertabs parsed content after the original container, and remove original container.
	$cont.after($ht)
	$cont.hide();

	if (mw.config.get("wgHeaderTabsDisableDefaultToc")) {
		$toc.remove(); // demp the TOC from the DOM.
	} else {
		$toc.after('<br />'); // ad a break after it.
	}




	/////////////////////////////////////
	// Tab Creation & Feature Handling //
	/////////////////////////////////////

	// Initialize jQuery UI Tabs, and setup functions.
	var $tabs = $ht.tabs({
		activate: function( event, ui ) {
			// When a tab is activated or focused
			var hash = ui.newPanel.data('hash');
			if (mw.config.get("wgHeaderTabsUseHistory")) {
				window.location.hash = hash;
			}
			if (mw.config.get("wgHeaderTabsEditTabLink")) {
				setTabEditLink(hash);
			}
		},
		create: function( event, ui ) {
			// When the tab is created.
			if (mw.config.get("wgHeaderTabsEditTabLink")) {
				setTabEditLink(ui.panel.data('hash'));
			}
		}
	});


	// If page loaded with hash, follow it.
	selectTabFromHash(window.location.hash);

	// Link TOC Links to Tabs
	$(".toc ul a").each(function(){
		$(this).click(function() {
			selectTabFromHash(this.hash.replace(/([;&,\.\+\*\~':"\!\^$%@\[\]\(\)=>\|])/g, '\\$1'),this);
		});
	});

	//TabLinks
	$(".tabLink").click(function(){
		selectTabFromHash(this.hash.replace(/([;&,\.\+\*\~':"\!\^$%@\[\]\(\)=>\|])/g, '\\$1'),this);
	});

	// This isn't explicitaly needed, but should help provide some usability
	// to links that may be created manually by a Wiki Editor.
	if ("onhashchange" in window) {
		// If this event is supported, we will use it as a fallback
		// in attempts to catch any other hashed links that may be inside tabs.
    	window.addEventListener("hashchange",function(){
			selectTabFromHash(window.location.hash);
		});
	}

	//////////////////////
	// Helper Functions //
	//////////////////////

	/**
	 * Focus Tab Based on Hash
	 * @param  string hash
	 * @param  object self [The "this" object of a link clicked]
	 */
	function selectTabFromHash(hash,self) {
		var self = self || false;
		var tabId = $('div[data-hash="'+hash+'"]').attr('id');
		var index = $('#headerTabs a[href="#'+tabId+'"]').parent().index();
		if (index == -1) {
			if (self) {
				// Attempt to swap to the parent tab if a hash is
				// hit that doesn't relate to the tab.
				// THIS CURRENTLY DOESNT WORK FOR DIRECT LINKING, SINCE THE SELF PROPERTY ISNT PASSED.
				var parentHash = $(self).parents('li').find('a').attr('href');
				if (parentHash) {
					var parentTabId = $('div[data-hash="'+parentHash+'"]').attr('id');
					var index = $('#headerTabs a[href="#'+parentTabId+'"]').parent().index();
				}
			} else {
				return;
			}
		}
		$tabs.tabs("option", "active", index);
	}

	/**
	 * Set the edit links for a tab based off its hash
	 * @param hash
	 */
	function setTabEditLink(hash) {
		$htedit.html(''); // clear it first
		if(typeof editLinkStore[hash] !== 'undefined') {
			$htedit.html(editLinkStore[hash]);
		}
	}

	/**
	 * Logger Function used for debugging... since MediaWiki's debug is broken.
	 * @param  mixed thing
	 */
	function log(thing) {
		var $log = $("#headerTabsContainerDebug");
		$log.show(); // log is hidden until this fires.

		// Covert non strings into JSON strings.
		if (typeof thing !== "string") { thing = JSON.stringify(thing); }

		// This is basically PHP's htmlspecialchars() -- Allow chars to display instead
		// of being parse by the browser into HTML.
		var map = {'&': '&amp;','<': '&lt;','>': '&gt;','"': '&quot;',"'": '&#039;'};
		thing = thing.replace(/[&<>"']/g, function(m) { return map[m]; });

		// Prepend to the log. Newest up top.
		$log.append(thing+"<hr />");
	}
});