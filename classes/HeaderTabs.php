<?php
use Symfony\Component\DomCrawler\Crawler;

/**
 * Header Tabs
 * Turn table of contents into tabbed contents using standard headers.
 *
 * @license		GNU General Public License v2.0 or later
 * @package		HeaderTabs
 * @link		https://gitlab.com/hydrawiki
 *
**/

class HeaderTabs {

    public static function tag( $input, array $args, Parser $parser, PPFrame $frame ) {
        if (empty($input)) {
            // If input is empty, we assume they used "<HeaderTabs />" to
            // Designate the end of tabbing. Original HeaderTabs Functionality
            // This tag, besides just enabling tabs, also designates
            // the end of tabs. Can be used even if automatiic namespaced
            $output = '<div id="nomoretabs"></div>';
        } else {
            if ($args['agressive'] == true) {
                $input = strip_tags($input);
            }
            $output = '<!--HEADER-TAB-SPLIT-->'.$parser->recursiveTagParse($input,$frame).'<!--HEADER-TAB-SPLIT-->';
        }
        return $output;
    }

    public static function renderSwitchTabLink( &$parser, $tabName, $linkText, $anotherTarget = '' ) {

        $tabTitle = Title::newFromText( $tabName );
        $tabKey = $tabTitle->getDBkey();
        $sanitizedLinkText = $parser->recursiveTagParse( $linkText );

        if ( $anotherTarget != '' ) {
            $targetTitle = Title::newFromText( $anotherTarget );
            $targetURL = $targetTitle->getFullURL();
            $output = '<a href="' . $targetURL . '#' . $tabKey . '">' . $sanitizedLinkText . '</a>';
        } else {
            $output = '<a href="#' . $tabKey . '" class="tabLink">' . $sanitizedLinkText . '</a>';
        }

        return $parser->insertStripItem( $output, $parser->mStripState );
    }

    public static function modifyOutput( &$parser, &$text ) {
        global $wgHeaderTabsAutomaticNamespaces, $wgHeaderTabsDefaultFirstTab;

		// Remove spans added if "auto-number headings" is enabled.
		$simplifiedText = preg_replace( '/\<span class="mw-headline-number"\>\d*\<\/span\>/', '', $text );

        // See if we have '<!--HEADER-TAB-SPLIT-->' comments..
        $splitOnComments = explode('<!--HEADER-TAB-SPLIT-->', $simplifiedText);
        if (count($splitOnComments) > 1) {
            // We have encapselated tabs.
            $above = $splitOnComments[0];
            $below = isset($splitOnComments[2])?$splitOnComments[2]:"";
            $modifyContent = trim($splitOnComments[1]);
        } else {
    		// Where do we stop rendering tabs, and what is below it?
    		// if we don't have a stop point, then bail out
    		$aboveandbelow = explode( '<div id="nomoretabs"></div>', $simplifiedText, 2 );
    		if ( count( $aboveandbelow ) <= 1 ) {
    			if ( in_array( $parser->getTitle()->getNamespace(), $wgHeaderTabsAutomaticNamespaces ) ) {
    				// We'll act as if the end of article is
    				// nomoretabs.
    				$aboveandbelow[] = '';
    			} else {
    				return true; // <headertabs/> tag is not found
    			}
    		}
    		$below = isset($aboveandbelow[1])?$aboveandbelow[1]:"";
            $modifyContent = trim($aboveandbelow[0]);

            $tabpatternsplit = '/(<h1.+?<span[^>]+class="mw-headline"[^>]+id="[^"]+"[^>]*>\s*.*?\s*<\/span>.*?<\/h1>)/';
    		$parts = preg_split( $tabpatternsplit, trim($modifyContent), -1, PREG_SPLIT_DELIM_CAPTURE );
            if ( isset($wgHeaderTabsDefaultFirstTab) && $wgHeaderTabsDefaultFirstTab !== FALSE && $parts[0] !== '' ) {
            	$above = ''; // explicit
    		} else {
    			$above = $parts[0];
    		}

        }

        if ( isset($wgHeaderTabsDefaultFirstTab) && $wgHeaderTabsDefaultFirstTab !== FALSE ) {
            $headline = '<h1><span class="mw-headline" id="'.str_replace(' ', '_', $wgHeaderTabsDefaultFirstTab).'">'.$wgHeaderTabsDefaultFirstTab.'</span></h1>';
            $modifyContent = $headline . $modifyContent;
        }

        $dom = new simple_html_dom();
        $dom->load($modifyContent);

        $headers = $dom->find('h1 > span.mw-headline');

        foreach ($headers as $i => $h) {
            $h1 = $h->parent();
            $wrapper = $h1->parent();
            if ($wrapper->tag !== "root") {
                // great, we are doing some shitty html
                if (trim($h1->outertext) == trim($wrapper->innertext)) {
                    // Only thing in the div is the title.
                    $next = $wrapper->next_sibling();
                    $stop = 0;
                    $loopFailSafe = 0;
                    while (!$stop) {
                        if ($next) {
                            $prev = $next; // refernece in next loop if next no longer defined.
                            $finder = $next->find('span.mw-headline', 0);
                            if (!$finder) {
                                // valid content to add to tab...
                                $next = $next->next_sibling();
                            } else {
                                // found another header. Wrap this section up.
                                $wrapper->outertext = "<section>".$wrapper->outertext;
                                $end = $next->prev_sibling();
                                $end->outertext = $end->outertext. "</section>";
                                $stop = 1;
                            }
                        } else {
                            // no next sibling!
                            $wrapper->outertext = "<section>".$wrapper->outertext;
                            $end = $prev;
                            $end->outertext = $end->outertext. "</section>";
                            $stop = 1;
                        }
                        $loopFailSafe++;
                        if ($loopFailSafe > 20) {
                            continue;
                        }
                    }
                } else {
                    $wrapper->outertext = "<section>".$wrapper->outertext."</section>";
                }
            } else {
                if ($i == 0) {
                    $h1->outertext = "<section>".$h1->outertext;
                } else {
                    $h1->outertext = "</section><section>".$h1->outertext;
                    if ($i + 1 == count($headers)) {
                        $dom = $dom . "</section>";
                    }
                }
            }
        }

		$text = $above
				. "<div id=\"headerTabsContainer\">".$dom."</div>"
				. "<pre id=\"headerTabsContainerDebug\" style=\"display: none;\"></pre>"
				. $below;

		return true;
    }

}
